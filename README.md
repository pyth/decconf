[![Coverage Status](https://coveralls.io/repos/bitbucket/pyth/decconf/badge.svg?branch=develop)](https://coveralls.io/bitbucket/pyth/decconf?branch=develop)

# README #

### What is this repository for? ###

* Python interface for configuring loconet decoders
* 0.1

### How do I get set up? ###

* Clone the repository
* run 
```
#!bash
./setup.py install

```

### Contribution guidelines ###

We try to keep close to PEP-0008

### Who do I talk to? ###

* Repo owner or admin