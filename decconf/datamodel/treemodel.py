from PySide import QtCore


class TreeModel(QtCore.QAbstractItemModel):
	def __init__(self, data, parent=None):
		super(TreeModel, self).__init__(parent)
		self.rootItem = TreeItem(("Title", "Summary"))
		self.setupModelData(data.split('\n'), self.rootItem)

	def columnCount(self, parent):
		if parent.isValid():
			return parent.internalPointer().columnCount()
		else:
			return self.rootItem.columnCount()

	def data(self, index, role):
		if not index.isValid():
			return None
		if role != QtCore.Qt.DisplayRole:
			return None
		item = index.internalPointer()
		return item.data(index.column())

	def flags(self, index):
		if not index.isValid():
			return QtCore.Qt.NoItemFlags
		return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

	def headerData(self, section, orientation, role):
		if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
			return self.rootItem.data(section)
		return None

	def index(self, row, column, parent):
		if not self.hasIndex(row, column, parent):
			return QtCore.QModelIndex()
		if not parent.isValid():
			parentItem = self.rootItem
		else:
			parentItem = parent.internalPointer()
		childItem = parentItem.child(row)
		if childItem:
			return self.createIndex(row, column, childItem)
		else:
			return QtCore.QModelIndex()

	def parent(self, index):
		if not index.isValid():
			return QtCore.QModelIndex()
		childItem = index.internalPointer()
		parentItem = childItem.parent()
		if parentItem == self.rootItem:
			return QtCore.QModelIndex()
		return self.createIndex(parentItem.row(), 0, parentItem)

	def rowCount(self, parent):
		if parent.column() > 0:
			return 0
		if not parent.isValid():
			parentItem = self.rootItem
		else:
			parentItem = parent.internalPointer()
		return parentItem.childCount()

	def setupModelData(self, lines, parent):
		parents = [parent]
		indentations = [0]
		number = 0
		while number < len(lines):
			position = 0
			while position < len(lines[number]):
				if lines[number][position] != ' ':
					break
				position += 1
			lineData = lines[number][position:].strip()
			if lineData:
				# Read the column data from the rest of the line.
				columnData = [s for s in lineData.split('\t') if s]
				if position > indentations[-1]:
					# The last child of the current parent is now the new
					# parent unless the current parent has no children.
					if parents[-1].childCount() > 0:
						parents.append(parents[-1].child(parents[-1].childCount() - 1))
						indentations.append(position)
				else:
					while position < indentations[-1] and len(parents) > 0:
						parents.pop()
						indentations.pop()
				# Append a new item to the current parent's list of children.
				parents[-1].appendChild(TreeItem(columnData, parents[-1]))
			number += 1

	def addRootItem(self, item):
		self.beginInsertRows(None, len(self.rootItem), len(self.rootItem));
		self.rootItem.append(item);
		self.endInsertRows(None, len(self.rootItem), len(self.rootItem));
	
	def addChildItem(self, item, parent = None):
		if parent is None:
			return self.addRootItem(item);
		
		self.beginInsertRows(parent, parent.childCount(), parent.childCount())q;
		parent.append(item);
		self.endInsertRows(parent, parent.childCount(), parent.childCount());
		
class TreeItem(object):
	def __init__(self, data, parent=None):
		self.parentItem = parent
		self.itemData = data
		self.childItems = []

	def appendChild(self, item):
		self.childItems.append(item)

	def child(self, row):
		return self.childItems[row]

	def childCount(self):
		return len(self.childItems)

	def columnCount(self):
		return len(self.itemData)

	def data(self, column):
		try:
			return self.itemData[column]
		except IndexError:
			return None

	def parent(self):
		return self.parentItem

	def row(self):
		if self.parentItem:
			return self.parentItem.childItems.index(self)
		return 0